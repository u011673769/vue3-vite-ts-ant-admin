import { createRouter, createWebHistory } from 'vue-router';
//路由配置
export const modules = [
  {
    path: 'main',
    name: '首页',
    component: () => import('@/views/main.vue'),
  },
  {
    path: 'user',
    name: '用户模块',
    component: () => import('@/views/manager/user.vue'),
  },
  {
    path: 'product',
    name: '课程模块',
    children: [
      {
        path: 'list',
        name: '课程管理',
        component: () => import('@/views/manager/product/list.vue'),
      },
      {
        path: 'add',
        name: '添加课程',
        component: () => import('@/views/manager/product/add.vue'),
      },
    ],
  },
];

// 路由配置
export const routes = [
  {
    path: '/login',
    name: '登录页面',
    component: () => import('@/views/login.vue'),
  },
  {
    path: '/manager',
    name: '后台管理页面',
    component: () => import('@/views/manager.vue'),
    children: modules,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

// 前置路由守卫
// router.beforeEach((to, from, next) => {
//   const { isLogin } = $(useAuth());
//   if (!to.path.includes('login')) {
//     if (!isLogin) next('/login');
//     else next();
//   } else {
//     if (isLogin) next('/');
//     else next();
//   }
// });

export default router;

