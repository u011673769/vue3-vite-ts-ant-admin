import { checkAdminRole, adminLogin } from '@/api/user';
import { message } from 'ant-design-vue';
import router from '@/router';

export function useAuth() {
  // token响应式设置本地存储
  const token = useLocalStorage('token', '');
  const personalInfo = useLocalStorage('personalInfo', '');
  const isLogin = computed(() => !!token.value);

  // 登录的方法
  async function login(phone: string, password: string) {
    // 调用登录接口
    const { code, data } = await adminLogin({ phone, password });

    if (code === 0) {
      token.value = data;
      // 获取用户信息
      const { data: details, code: c_code } = await checkAdminRole();
      if (c_code === 0) {
        if (details.role === 'ADMIN') {
          router.push('/manager/main');
          message.success('登录成功');
          personalInfo.value = JSON.stringify(details)
        } else {
          clearLoginState();
          message.error('您不是管理员, 无法登录!');
        }
      }
    }
  }

  // 清除token
  function clearLoginState() {
    token.value = '';
    router.push('/login');
  }

  return {
    token,
    isLogin,
    login,
    clearLoginState,
    personalInfo
  };
}

