import request, { BASE_URL } from '@/utils/request';

/**
 *  获取实时概况
 */
export function sum() {
    return request('/sum', {
        baseURL: BASE_URL + '/api/admin/main/v1',
      });
  };
  
/**
 *  交易概况曲线图
 */
export function orderSummary() {
    return request('/summary/order', {
        baseURL: BASE_URL + '/api/admin/main/v1',
      });
  };
  
/**
 *  流量统计
 */
export function flowSummary() {
    return request('/summary/flow', {
        baseURL: BASE_URL + '/api/admin/main/v1',
      });
  };
  
/**
 *  客户端统计
 */
export function clientSummary() {
    return request('/summary/client', {
        baseURL: BASE_URL + '/api/admin/main/v1',
      });
  };
  