import request, { BASE_URL } from '@/utils/request';

// 登录接口
export function adminLogin(data: {
  phone: string;
  code?: string;
  password?: string;
  client?: string;
}) {
  return request('/login', {
    method: 'POST',
    data,
    baseURL: BASE_URL + '/api/user/v1',
  });
}

// 用户数据接口
export function checkAdminRole() {
  return request('/detail', {
    baseURL: BASE_URL + '/api/user/v1',
  });
}

//==========  后台管理
/**
 * 用户列表
 */
export function queryAccountList(data: { 
  page: number;
  size: number;
  id?: string;
  keywords?: string;
}) {
  return request('/list', {
    method: 'POST',
    data,
    baseURL: BASE_URL + '/api/admin/account/v1',
  });
}