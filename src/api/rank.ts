import request, { BASE_URL } from '@/utils/request';

/**
 * 获取热门商品榜数据
 */
export function getHotProduct() {
    return request('hot_product', {
        baseURL: BASE_URL + '/api/rank/v1/',
    });
};

/**
 * 获取卷王排行榜数据
 */
export function getDuration() {
    return request('/duration', {
        baseURL: BASE_URL + '/api/rank/v1/',
    });
};

