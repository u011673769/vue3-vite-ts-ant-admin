import { message } from 'ant-design-vue';
import axios, { type AxiosRequestConfig } from 'axios';
//后端请求地址
export const BASE_URL = 'http://127.0.0.1:8081';
const request = axios.create({ baseURL: `${BASE_URL}/api/admin/v1` });

// 请求拦截器
request.interceptors.request.use((config) => {
  const { token } = $(useAuth());
  if (token) {
    config.headers.token = token;
  }
  return config;
});

// 响应拦截器
request.interceptors.response.use((response) => {
  const { clearLoginState } = $(useAuth());
  const data = response.data;

  if (data.code !== 0) {
    message.error(data.msg);
    clearLoginState();
  }
  return response;
});

export default async function (url: string, options?: AxiosRequestConfig) {
  return (
    await request({
      url,
      ...options,
    })
  ).data;
}

