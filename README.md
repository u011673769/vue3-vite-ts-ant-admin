## 开箱即用中后台管理模板
本框架采用的是主流技术栈：vue3+vite+ant-design-vue+ts+unoncss+axios+echarts4.x 等；
源码内已提供相关案例，组件封装案例，http请求后端接口案例；适合生产开发和联系前端；
开发工具和技术：vscode + node v15.x以上

## 框架结构说明

框架入门说明文档
![WPS图片(1).png](https://raw.gitcode.com/u011673769/vue3-vite-ts-ant-admin/attachment/uploads/04480fbd-159f-4b2c-aa83-3ababb1278f8/WPS图片_1_.png 'WPS图片(1).png')
主要文件说明：
api：存放后端接口文件，进行统一管理；
components：存放组件文件，已在vite.config.ts内配置自动导入，详细使用说明见技术栈；
composables：存放所有需要使用pinia持久化功能的文件管理；
router：页面管理，项目内所有页面的配置和跳转管理；
utils：常用工具类管理，目前存放了axios的二次封装工具类；
views：项目页面管理，实际对外提供访问的页面，如：登录页面，主页等。
env.d.ts：env.d.ts是环境变量文件，主要是让TS认知Vue文件
tsconfig.config.json：配置vite的各种工具运行在node环境中
tsconfig.json：配置vue（src文件夹）运行浏览器中


## 关于使用
解压项目包，使用vscode导入项目；如下效果

在命令行中输入：yarn 或 npm i 等待下载完成依赖
启动命令：yarn dev 或 npm dev
打开浏览器访问：http://127.0.0.1:5173/

首页地址：http://127.0.0.1:5173/main


## 技术栈列表
vue3
ant-design-vue
unocss
axios
echarts
pinia
unplugin-vue-jsx
vue-router
typescript
unplugin-auto-import
unplugin-vue-components

## 主要技术内容说明
1、vue3
vue3的setup语法糖和组合式api的使用；其中主要是ref函数 和 reactive函数 的使用，对象的定义不再需要像vue2之前的写法，可以将变量像js的原生写法一样，统一定义在最顶部，方便查看和管理；总体来说vue3的写法，让开发人员可以比较个性化的编写自己的代码规则；

2、Unocss
页面CSS简化利器，传统的css编写都需要在Style标签内定好，使用Unocss可以在组件上直接定义，且兼容效果也很好；嘿嘿，前提还是需要有css基础的人使用起来才可以得心应手！

看以下代码达到的是同样效果；

Unocss框架的编写格式首次接触肯定会不适应，这里还提供了一个格式写法工具可以学习：
https://uno.antfu.me/

只需要输入css的相关属性，系统会自动提示编写格式

资料参考文献：https://zhuanlan.zhihu.com/p/425814828?utm_medium=social&utm_oi=31225867665408
3、Axios
框架内做了二次封装，灵活的支持多种请求方式（POST/GET/DELETE/PUT等），且做了token认证授权，登录拦截；且还有每种请求方式的调用示例，这就很方便了。

4、Pinia
Vue新一代状态管理插件Pinia，这比vue的Store是方便和直观多了。使用它之后，以后就不用Store的写法了。
优点：
使用直观，编写store更加容易
完整的 TypeScript 支持
Pinia的体积极小，包体积仅为约1kb
抛弃了Vuex中的Mutation，使用action支持同步和异步
官方地址：https://pinia.web3doc.top/

5、unplugin-auto-import 和 unplugin-vue-components
两个都是提高开发效率的插件，自动引入插件/组件；当你编写一个组件之后，vue写法是在你需要使用的页面都要使用import 进行手动导入；而配置以上两个插件之后，你只需要告诉他哪个文件夹需要进行自动导入组件，之后在需要使用的页面，直接写文件名即可引入组件；



花费一天内的时间就可以熟悉整个前端结构的开发。

如果有疑问或问题，请随时保持联系；作者也可以提供项目内后端数据接口供看完成效果和学习测试；
微信：dz50699


## 初始化

```sh
npm install
```

### 开发模式

```sh
npm run dev
```

### 打包编译

```sh
npm run build
```


